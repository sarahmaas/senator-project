<?php
/*
Filename:     index.php
Authors:      Sarah Maas/Donald Elliott
Class:        CS340-400
Project:      Database Final Project
Description:  Main page for the final project.
*/
  include('dbhook.php');
  include('selectSpecificVotes.php');
  ini_set('display_errors', 'On');
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PSWD, DB_NAME);
  if($mysqli->connect_errno){
  	echo "Connection error " . $mysqli->connect_errno . " " . $mysqli->connect_error;
  	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link href="lg-map/map.css" rel="stylesheet" type="text/css" />
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="lg-map/raphael.js" type="text/javascript"></script>
  <script src="lg-map/scale.raphael.js" type="text/javascript"></script>
  <script src="lg-map/lg-map.js" type="text/javascript"></script>
  <script src="js/loadVotes.js"></script>
  <script src="js/typed.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <!-- Page Layout -->
  <!-- Navigation Menu -->
<ul>
  <li><a href="/~elliotdo/DB/adminPage.php">Add Data</a></li>
  <li><a href="/~elliotdo/DB/authors.html">Authors</a></li>
  <li><a href="#">Home</a></li>
</ul>

 <!-- Main page layout starts here -->
  <div class="container">
    <div class="row">
      <div class="one-half column">
        <h5><i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i><span class="element">
        </span></h5>
        <p>If you already know the name of a Senator you'd like to check, feel free to use the search box. You can search
          for a first or last name. If you don't know their name or
          are just curious about other state Senator's records, use the interactive map below.</p>
      </div>
      <div class="one-half column" style="text-align:right">
        <center>
          <br />
        <form action="search.php" method="POST" style="text-align:right">
          <input type="text" name="query" placeholder="First Or Last Name"/>
          <input type="submit" value="Search Name"/>
          <p style="color:#09a2ff;">ONLY ONE KEYWORD SEARCH IS SUPPORTED</p>
          </center>
        </form>
          <div>
          <!-- FORM USED FOR THE FILTERING OF VOTES BASED UPON POLITICAL PARTY-->
        	<form method="post" action="filter.php">
        		<fieldset>
        				<select name="Bill">
        					<?php
        					if(!($stmt = $mysqli->prepare("SELECT id, name FROM bills"))){
        						echo "Prepare failed: "  . $stmt->errno . " " . $stmt->error;
        					}
        					if(!$stmt->execute()){
        						echo "Execute failed: "  . $mysqli->connect_errno . " " . $mysqli->connect_error;
        					}
        					if(!$stmt->bind_result($id, $pname)){
        						echo "Bind failed: "  . $mysqli->connect_errno . " " . $mysqli->connect_error;
        					}
        					 echo '<option value=" -1 "> ' . 'SHOW ALL' . '</option>\n';
        					while($stmt->fetch()){
        					 echo '<option value="'. $id . '"> ' . $pname . '</option>\n';
        					}
        					$stmt->close();
        					?>
        				</select>
                <input type="submit" value="Run Filter" /><br />
                <p style="color:#09a2ff;">FILTER PARTY VOTES BY BILL</p>
        		</fieldset>
        	</form>
        </div>
      </div>
    </div>
  </div>
</form>
  <br />
  <center>
  <!--
      ALL ITEMS BELOW THIS COMMENT LINE REFERENCE THE JAVASCRIPT MAP.
      ONCE A STATE IS CLICKED IT TRIGGERS THE SENATORS TO LOAD FROM usa.js.
      Once those are loaded the user may click the senator name which then
      loads their voting record via php.
  -->
  <div class="container">
    <div class="row">
      <div class="twelve.columns">
        <div class="lg-map-wrapper" data-map="lg-map/usa.js">
          <div class="lg-map-text"></div>
            <div id="lg-map">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container" id="s1">
    <img src="images/SenatorPics/rshelby.jpg">
    <h4>Richard Shelby Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s2">
    <img src="images/SenatorPics/lmurkowski.jpg">
    <h4>Lisa Murkowski Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query1)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s3">
    <img src="images/SenatorPics/dsullivan.jpg">
    <h4>Dan Sullivan Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query2)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s4">
    <img src="images/SenatorPics/jmccain.jpg">
    <h4>John McCain Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query3)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s5">
    <img src="images/SenatorPics/jflake.jpg">
    <h4>Jeff Flake Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query4)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s6">
    <img src="images/SenatorPics/jboozman.jpg">
    <h4>John Boozman Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query5)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s7">
    <img src="images/SenatorPics/tcotton.jpg">
    <h4>Tom Cotton Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query6)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s8">
    <img src="images/SenatorPics/dfeinstein.jpg">
    <h4>Dianne Feinstein Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query7)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s9">
    <img src="images/SenatorPics/kharris.jpg">
    <h4>Kamala Harris Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query8)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s10">
    <img src="images/SenatorPics/mbennet.jpg">
    <h4>Michael Bennet Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query9)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s11">
    <img src="images/SenatorPics/cgardner.jpeg">
    <h4>Cory Gardner Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query10)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s12">
    <img src="images/SenatorPics/rblumenthal.jpg">
    <h4>Richard Blumenthal Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query11)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s13">
    <img src="images/SenatorPics/cmurphy.jpg">
    <h4>Chris Murphy Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query12)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s14">
    <img src="images/SenatorPics/tcarper.jpg">
    <h4>Tom Carper Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query13)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s15">
    <img src="images/SenatorPics/ccoons.jpg">
    <h4>Chris Coons Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query14)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s16">
    <img src="images/SenatorPics/bnelson.jpg">
    <h4>Bill Nelson Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query15)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s17">
    <img src="images/SenatorPics/mrubio.jpg">
    <h4>Marco Rubio Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query16)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s18">
    <img src="images/SenatorPics/jisakson.jpg">
    <h4>Johnny Isakson Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query17)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s19">
    <img src="images/SenatorPics/dperdue.jpg">
    <h4>David Perdue Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query18)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s20">
    <img src="images/SenatorPics/bschatz.jpg">
    <h4>Brian Schatz Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query19)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s21">
    <img src="images/SenatorPics/mhirono.jpg">
    <h4>Mazie Hirono Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query20)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s22">
    <img src="images/SenatorPics/mcrapo.jpg">
    <h4>Mike Crapo Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query21)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s23">
    <img src="images/SenatorPics/jrisch.jpg">
    <h4>Jim Risch Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query22)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s24">
    <img src="images/SenatorPics/ddurbin.jpg">
    <h4>Dick Durbin Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query23)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s25">
    <img src="images/SenatorPics/tduckworth.jpg">
    <h4>Tammy Duckworth Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query24)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s26">
    <img src="images/SenatorPics/jdonnelly.jpg">
    <h4>Joe Donnelly Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query25)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s27">
    <img src="images/SenatorPics/tyoung.jpg">
    <h4>Todd Young Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query26)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s28">
    <img src="images/SenatorPics/cgrassley.jpg">
    <h4>Chuck Grassley Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query27)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s29">
    <img src="images/SenatorPics/jernst.jpg">
    <h4>Joni Ernst Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query28)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s30">
    <img src="images/SenatorPics/proberts.jpg">
    <h4>Pat Roberts Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query29)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s31">
    <img src="images/SenatorPics/jmoran.jpg">
    <h4>Jerry Moran Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query30)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s32">
    <img src="images/SenatorPics/mmcconnell.jpg">
    <h4>Mitch McConnell Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query31)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s33">
    <img src="images/SenatorPics/rpaul.jpg">
    <h4>Rand Paul Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query32)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s34">
    <img src="images/SenatorPics/bcassidy.jpg">
    <h4>Bill Cassidy Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query33)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s35">
    <img src="images/SenatorPics/jkennedy.jpg">
    <h4>John Kennedy Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query34)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s36">
    <img src="images/SenatorPics/scollins.jpg">
    <h4>Susan Collins Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query35)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s37">
    <img src="images/SenatorPics/aking.jpg">
    <h4>Angus King Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query36)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s38">
    <img src="images/SenatorPics/bcardin.jpg">
    <h4>Ben Cardin Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query37)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s39">
    <img src="images/SenatorPics/vanhollen.jpg">
    <h4>Chris Van Hollen Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query38)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s40">
    <img src="images/SenatorPics/ewarren.jpg">
    <h4>Elizabeth Warren Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query39)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s41">
    <img src="images/SenatorPics/emarkey.jpg">
    <h4>Ed Markey Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query40)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s42">
    <img src="images/SenatorPics/dstabenow.jpg">
    <h4>Debbie Stabenow Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query41)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s43">
    <img src="images/SenatorPics/gpeters.jpg">
    <h4>Gary Peters Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query42)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s44">
    <img src="images/SenatorPics/aklobuchar.jpg">
    <h4>Amy Klobuchar Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query43)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s45">
    <img src="images/SenatorPics/afranken.jpg">
    <h4>Al Franken Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query44)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s46">
    <img src="images/SenatorPics/tcochran.jpg">
    <h4>Thad Cochran Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query45)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s47">
    <img src="images/SenatorPics/rwicker.jpg">
    <h4>Roger Wicker Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query46)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s48">
    <img src="images/SenatorPics/cmccaskill.jpg">
    <h4>Claire McCaskill Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query47)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s49">
    <img src="images/SenatorPics/rblunt.jpg">
    <h4>Roy Blunt Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query48)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s50">
    <img src="images/SenatorPics/jtester.jpg">
    <h4>Jon Tester Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query49)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s51">
    <img src="images/SenatorPics/sdaines.jpg">
    <h4>Steve Daines Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query50)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s52">
    <img src="images/SenatorPics/dfischer.jpg">
    <h4>Deb Fischer Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query51)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s53">
    <img src="images/SenatorPics/bsasse.jpg">
    <h4>Ben Sasse Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query52)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s54">
    <img src="images/SenatorPics/dheller.jpg">
    <h4>Dean Heller Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query53)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s55">
    <img src="images/SenatorPics/cortezmasto.jpg">
    <h4>Catherine Cortez Masto Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query54)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s56">
    <img src="images/SenatorPics/jshaheen.jpg">
    <h4>Jeanne Shaheen Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query55)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s57">
    <img src="images/SenatorPics/mhassan.jpg">
    <h4>Maggie Hassan Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query56)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s58">
    <img src="images/SenatorPics/bmenendez.jpg">
    <h4>Bob Menendez Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query57)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s59">
    <img src="images/SenatorPics/cbooker.jpg">
    <h4>Cory Booker Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query58)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s60">
    <img src="images/SenatorPics/tudall.jpg">
    <h4>Tom Udall Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query59)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s61">
    <img src="images/SenatorPics/mheinrich.jpg">
    <h4>Martin Heinrich Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query60)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s62">
    <img src="images/SenatorPics/cschumer.jpg">
    <h4>Chuck Schumer Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query61)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s63">
    <img src="images/SenatorPics/kgillibrand.jpg">
    <h4>Kirsten Gillibrand Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query62)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s64">
    <img src="images/SenatorPics/rburr.jpg">
    <h4>Richard Burr Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query63)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s65">
    <img src="images/SenatorPics/ttillis.jpg">
    <h4>Thom Tillis Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query64)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s66">
    <img src="images/SenatorPics/jhoeven.JPG">
    <h4>John Hoeven Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query65)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s67">
    <img src="images/SenatorPics/hheitkamp.jpg">
    <h4>Heidi Heitkamp Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query66)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s68">
    <img src="images/SenatorPics/sbrown.jpg">
    <h4>Sherrod Brown Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query67)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s69">
    <img src="images/SenatorPics/rportman.jpg">
    <h4>Rob Portman Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query68)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s70">
    <img src="images/SenatorPics/jinhofe.jpg">
    <h4>Jim Inhofe Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query69)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s71">
    <img src="images/SenatorPics/jlankford.jpg">
    <h4>James Lankford Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query70)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s72">
    <img src="images/SenatorPics/rwyden.jpg">
    <h4>Ron Wyden Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query71)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s73">
    <img src="images/SenatorPics/jmerkley.jpg">
    <h4>Jeff Merkley Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query72)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s74">
    <img src="images/SenatorPics/caseyjr.jpg">
    <h4>Bob Casey Jr. Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query73)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s75">
    <img src="images/SenatorPics/ptoomey.jpg">
    <h4>Pat Toomey Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query74)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s76">
    <img src="images/SenatorPics/jreed.jpg">
    <h4>Jack Reed Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query75)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s77">
    <img src="images/SenatorPics/swhitehouse.jpg">
    <h4>Sheldon Whitehouse Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query76)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s78">
    <img src="images/SenatorPics/lgraham.jpg">
    <h4>Lindsey Graham Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query77)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s79">
    <img src="images/SenatorPics/tscott.jpg">
    <h4>Tim Scott Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query78)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s80">
    <img src="images/SenatorPics/jthune.jpg">
    <h4>John Thune Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query79)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s81">
    <img src="images/SenatorPics/mrounds.jpg">
    <h4>Mike Rounds Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query80)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s82">
    <img src="images/SenatorPics/lalexander.jpg">
    <h4>Lamar Alexander Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query81)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s83">
    <img src="images/SenatorPics/bcorker.jpg">
    <h4>Bob Corker Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query82)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s84">
    <img src="images/SenatorPics/jcornyn.jpg">
    <h4>John Cornyn Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query83)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s85">
    <img src="images/SenatorPics/tcruz.jpg">
    <h4>Ted Cruz Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query84)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s86">
    <img src="images/SenatorPics/ohatch.jpg">
    <h4>Orrin Hatch Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query85)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s87">
    <img src="images/SenatorPics/mlee.jpg">
    <h4>Mike Lee Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query86)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s88">
    <img src="images/SenatorPics/pleahy.jpg">
    <h4>Patrick Leahy Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query87)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s89">
    <img src="images/SenatorPics/bsanders.jpg">
    <h4>Bernie Sanders Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query88)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s90">
    <img src="images/SenatorPics/mwarner.jpg">
    <h4>Mark Warner Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query89)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s91">
    <img src="images/SenatorPics/tkaine.jpg">
    <h4>Tim Kaine Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query90)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s92">
    <img src="images/SenatorPics/pmurray.jpg">
    <h4>Patty Murray Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query91)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s93">
    <img src="images/SenatorPics/mcantwell.jpg">
    <h4>Maria Cantwell Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query92)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s94">
    <img src="images/SenatorPics/jmanchin.jpg">
    <h4>Joe Manchin Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query93)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s95">
    <img src="images/SenatorPics/moorecapito.jpg">
    <h4>Shelley Moore Capito Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query94)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s96">
    <img src="images/SenatorPics/rjohnson.jpg">
    <h4>Ron Johnson Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query95)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s97">
    <img src="images/SenatorPics/tbaldwin.jpg">
    <h4>Tammy Baldwin Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query96)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s98">
    <img src="images/SenatorPics/menzi.jpg">
    <h4>Mike Enzi Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query97)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s99">
    <img src="images/SenatorPics/jbarrasso.jpg">
    <h4>John Barrasso Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query98)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="container" id="s100">
    <img src="images/SenatorPics/lstrange.jpg">
    <h4>Luther Strange Voting Record</h4>
    <table>
      <tr>
        <th>Bill Name</th>
        <th>Yea</th>
        <th>Nay</th>
        <th>No Vote</th>
      </tr>
      <?php while($row1 = mysqli_fetch_array($senatorVote_query99)):; ?>
      <tr>
        <td><?php echo $row1[0]; ?></td>
        <td><?php echo $row1[1]; ?></td>
        <td><?php echo $row1[2]; ?></td>
        <td><?php echo $row1[3]; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
<br />
<br />
  <div class="container">
    <div class="row">
      <div class="twelve.columns">
        <h6><i class="fa fa-copyright fa" aria-hidden="true"></i> 2017 <a href="http://www.rodelliott.com" class="white-text">Donald Elliott</a> & Sarah Maas</h6>
  </div>
</center>
<br />
<br />
<!-- End Document -->
<!-- PLUGIN CREATED BY MATT BOLDT (Typed Text Animation)
http://www.mattboldt.com/
-->
<script>
  $(function(){
      $(".element").typed({
        strings: ["Welcome to the CS340 Final Project"],
        typeSpeed: 150
      });
  });
</script>
<script>
    hideAllVotes();
</script>
</body>
</html>
