<?php
/*
Filename:     addSenVote.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Adds a senator vote to the database in the table senator_votes
*/
  //Get the submitted form
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Assign form names to variables
    $senator_id = $_POST['senator_id'];
    $bill_id = $_POST['bill_id'];
    $yeas = $_POST['yeas'];
    $nays = $_POST['nays'];
    $no_vote = $_POST['no_vote'];
    //Insert the new senator vote into the table
    $sqlinsert = "INSERT INTO senator_votes (senator_id, bill_id, yeas, nays, no_vote) VALUES ('$senator_id', '$bill_id', '$yeas', '$nays', '$no_vote')";
    $insert = $dbcon->query($sqlinsert);
    if(!$insert) {
      die("Error: {$dbcon->errno} : {$dbcon->error}");
    }
  }
  //Once senator vote is added go back to senatorVote.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/senatorVote.php");
?>
