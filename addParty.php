<?php
/*
Filename:     addParty.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Adds a party to the database in the table parties
*/
//Get the submitted form
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Assign name to name variable
    $name = $_POST['name'];
    $abbreviation = $_POST['abbreviation'];
    //Insert the new party into parties
    $sqlinsert = "INSERT INTO parties (name, abbreviation) VALUES ('$name', '$abbreviation')";
    $insert = $dbcon->query($sqlinsert);
    if(!$insert) {
      die("Error: {$dbcon->errno} : {$dbcon->error}");
    }
  }
  //Once party is added go back to party.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/party.php");
?>
