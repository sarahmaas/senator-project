<?php
/*

Filename:     sponsor.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to display the sponsors of bills.
*/
  include('dbhook.php');
  //Query used to display the bill id/name, and the sponsors id/name
  $sqlselect = "SELECT sponsors.bill_id, bills.name, sponsors.senator_id, senators.first_name, senators.last_name FROM sponsors
                INNER JOIN bills ON sponsors.bill_id = bills.id
                INNER JOIN senators ON sponsors.senator_id = senators.id
                ORDER BY sponsors.bill_id";
  $result = mysqli_query($dbcon, $sqlselect);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="js/typed.js"></script>
  <script src="js/showHide.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <!-- Page Layout -->
  <!-- Navigation Menu -->
<ul>
  <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
  <li><a href="party.php" style="cursor:pointer;">Party</a></li>
  <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
  <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
  <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
  <li><a href="state.php" style="cursor:pointer;">States</a><li>
  <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
</ul>
<center>
  <div class="container">
    <div class="row">
        <div class="twelve.columns">
          <!--
            Start the Add Bill Section. Will use a post and utilize addBill.php
          -->
          <h2>Add Sponsor</h2>
          <!-- Using POST and addSponsor.php to add to database -->
          <form method="post" action="addSponsor.php">
            <input type="hidden" name="submitted" value="true" />
            <div class="form-group">
              <label for="billIDNum">Bill ID</label><br />
              <input type="text" name="bill_id" class="form-control" id="billIDNum" placeholder=""><br />
              <label for="senIDNum">Senator ID</label><br />
              <input type="text" name="senator_id" class="form-control" id="senIDNum" placeholder=""><br />
            </div>
            <button type="submit" class="btn btn-default">ADD SPONSOR</button>
          </form>
          <div id="showTheBills">
          <h3>Bills</h3>
          <table>
            <tr>
              <th>Bill ID</th>
              <th>Bill Name</th>
              <th>Senator ID</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
            <?php while($data = mysqli_fetch_array($result)):; ?>
            <tr>
              <!--Get the Bill ID from the array-->
              <td><?php echo $data[0]; ?></td>
              <!--Get the Bill Name from the array-->
              <td><?php echo $data[1]; ?></td>
              <!--Get the senator id-->
              <td><?php echo $data[2]; ?></td>
              <!--Get the senator first name-->
              <td><?php echo $data[3]; ?></td>
              <!--Get the senator last name-->
              <td><?php echo $data[4]; ?></td>
            </tr>
            <?php endwhile; ?>
          </table>
        </div>
        </div>
      </div>
    </div>
</center>
<footer>
  <center>
    <br />
  <span>©2017 Donald Elliott & Sarah Maas</span><br />
  <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
  <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
  <br />
</center>
</footer>
<script>
</script>
</body>
</html>
