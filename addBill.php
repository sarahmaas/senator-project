<?php
/*
Filename:     addBill.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Adds a bill to the database in the table bills
*/
  //Get the submitted form
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Assign name to name variable
    $name = $_POST['name'];
    //Insert the new bill into bills
    $sqlinsert = "INSERT INTO bills (name) VALUES ('$name')";
    $insert = $dbcon->query($sqlinsert);
    if(!$insert) {
      die("Error: {$dbcon->errno} : {$dbcon->error}");
    }
  }
  //Once bill is added go back to bill.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/bill.php");
?>
