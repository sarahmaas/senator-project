<?php
/*
Filename:     party.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to display the parties table from the database.
              Will also allow the user to click a button to edit the entries.
*/
  include('dbhook.php');
  $sqlselect = "SELECT * FROM parties";
  $result = mysqli_query($dbcon, $sqlselect);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="js/typed.js"></script>
  <script src="js/showHide.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <!-- Page Layout -->
  <!-- Navigation Menu -->
<ul>
  <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
  <li><a href="party.php" style="cursor:pointer;">Party</a></li>
  <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
  <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
  <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
  <li><a href="state.php" style="cursor:pointer;">States</a><li>
  <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
</ul>
<center>
<div class="container">
  <div class="row">
      <div class="twelve.columns">
        <h2>Add Party</h2>
        <!-- Using a post and addParty.php -->
        <form method="post" action="addParty.php">
          <input type="hidden" name="submitted" value="true" />
          <div class="form-group">
            <label for="partyName">Name</label><br />
            <input type="text" name="name" class="form-control" id="partyName" placeholder="Democrat">
          </div>
          <div class="form-group">
            <label for="partyAbbr">Abbreviation</label><br />
            <input type="text" name="abbreviation" class="form-control" id="partyAbbr" placeholder="D">
          </div>
          <button type="submit" class="btn btn-default">ADD PARTY</button>
        </form>
        <div id="showTheParties">
        <h3>Parties</h3>
        <table>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Abbreviation</th>
            <th>Edit</th>
          </tr>
          <?php while($data = mysqli_fetch_array($result)):; ?>
          <tr>
            <!-- Show the data -->
            <td><?php echo $data[0]; ?></td>
            <td><?php echo $data[1]; ?></td>
            <td><?php echo $data[2]; ?></td>
            <!-- Pass the id to editParty.php if user wants to edit entry -->
            <td><a href="editParty.php?id=<?php echo $data[0]; ?>">Edit</a></td>
          </tr>
          <?php endwhile; ?>
        </table>
      </div>
     </div>
   </div>
 </div>
 </center>
 <footer>
   <center>
     <br />
   <span>©2017 Donald Elliott & Sarah Maas</span><br />
   <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
   <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
   <br />
 </center>
 </footer>
</body>
</html>
