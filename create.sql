-- CS 340
-- Final Project
-- Create all tables

DROP TABLE IF EXISTS senators;
DROP TABLE IF EXISTS votes;
DROP TABLE IF EXISTS senators;
DROP TABLE IF EXISTS bills;
DROP TABLE IF EXISTS states;
DROP TABLE IF EXISTS parties;

CREATE TABLE states (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	abbreviation VARCHAR(2) NOT NULL,
	CONSTRAINT unique_name UNIQUE (name, abbreviation)
) ENGINE = INNODB;

CREATE TABLE parties (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	abbreviation VARCHAR(2) NOT NULL,
	CONSTRAINT unique_name UNIQUE (name, abbreviation)
) ENGINE = INNODB;

CREATE TABLE bills (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	CONSTRAINT unique_name UNIQUE (name)
) ENGINE = INNODB;

CREATE TABLE senators (
	id INT NOT NULL AUTO_INCREMENT,
	party_id INT NOT NULL,
	state_id INT NOT NULL,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	active BOOLEAN NOT NULL,
	CONSTRAINT unique_name UNIQUE (first_name, last_name),
	FOREIGN KEY(party_id) REFERENCES parties(id),
	FOREIGN KEY(state_id) REFERENCES states(id),
	PRIMARY KEY (id)
) ENGINE = INNODB;

CREATE TABLE senator_votes (
	senator_id INT NOT NULL,
	bill_id INT NOT NULL,
	yeas INT,
	nays INT,
	no_vote INT,
	FOREIGN KEY (senator_id) REFERENCES senators(id),
	FOREIGN KEY (bill_id) REFERENCES bills(id),
	PRIMARY KEY (senator_id, bill_id)
) ENGINE = INNODB;

CREATE TABLE sponsors (
	bill_id INT NOT NULL,
	senator_id INT NOT NULL,
	FOREIGN KEY(bill_id) REFERENCES bills(id),
	FOREIGN KEY(senator_id) REFERENCES senators(id),
	PRIMARY KEY (bill_id, senator_id)
) ENGINE = INNODB;
