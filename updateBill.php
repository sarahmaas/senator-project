<?php
/*
Filename:     updateBill.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to update the bills.
*/
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Set names passed in to variables
    $id = $_POST['id'];
    $name = $_POST['name'];
    if ($name != "") {
      $sqlEdit = "UPDATE bills SET name='$name' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
  }
  //Once updated refresh back to bill.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/bill.php");
?>
