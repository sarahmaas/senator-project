<?php
/*
Filename:     senators.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to display the senators name, party, state, and whether or
              not they are active. Also allows the user to choose to edit the
              entry.
*/
  include('dbhook.php');
  $sqlselect = "SELECT senators.id, senators.first_name, senators.last_name, senators.party_id, parties.name, senators.state_id, states.name, senators.active FROM senators
                INNER JOIN parties ON senators.party_id = parties.id
                INNER JOIN states ON senators.state_id = states.id
                ORDER BY senators.id";
  $result = mysqli_query($dbcon, $sqlselect);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="js/typed.js"></script>
  <script src="js/showHide.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <!-- Page Layout -->
  <!-- Navigation Menu -->
<ul>
  <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
  <li><a href="party.php" style="cursor:pointer;">Party</a></li>
  <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
  <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
  <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
  <li><a href="state.php" style="cursor:pointer;">States</a><li>
  <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
</ul>
<center>
<div class="container">
  <div class="row">
      <div class="twelve.columns">
        <h2>Add Senator</h2>
        <form method="post" action="addSenator.php">
          <input type="hidden" name="submitted" value="true" />
          <div class="form-group">
            <label for="partyID">Party ID</label><br />
            <input type="text" name="party_id" class="form-control" id="partyID" placeholder="Party ID #">
          </div>
          <div class="form-group">
            <label for="stateID">State ID</label><br />
            <input type="text" name="state_id" class="form-control" id="stateID" placeholder="State ID #">
          </div>
          <div class="form-group">
            <label for="billYeas">First Name</label><br />
            <input type="text" name="first_name" class="form-control" id="firstName" placeholder="First Name">
          </div>
          <div class="form-group">
            <label for="billNays">Last Name</label><br />
            <input type="text" name="last_name" class="form-control" id="lastName" placeholder="Last Name">
          </div>
          <center>
            <!--Checkbox to make selection of Active status easier. Also to enforce constraint.-->
            <!-- Senator cannot be both active and inactive -->
            <h5>Is The Senator Active or Inactive?</h5>
            <input type="checkbox" id="activeStatus" class="small" value="1" name="active">
            <label for="yea">Active</label></input>
            <input type="checkbox" id="inactiveStatus" class="small" value="0" name="active">
            <label for="nay">Inactive</label></input>
          </center>
          <button type="submit" class="btn btn-default">ADD SENATOR</button>
        </form>
        <div id="showTheSenators">
        <h3>Senators</h3>
        <table>
          <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Party ID</th>
            <th>Party</th>
            <th>State ID</th>
            <th>State</th>
            <th>Active</th>
            <th>Edit</th>
          </tr>
          <?php while($data = mysqli_fetch_array($result)):; ?>
          <tr>
            <!--Display the data-->
            <td><?php echo $data[0]; ?></td>
            <td><?php echo $data[1]; ?></td>
            <td><?php echo $data[2]; ?></td>
            <td><?php echo $data[3]; ?></td>
            <td><?php echo $data[4]; ?></td>
            <td><?php echo $data[5]; ?></td>
            <td><?php echo $data[6]; ?></td>
            <td><?php echo $data[7]; ?></td>
            <!--Allow the user to edit the senator-->
          <td><a href="editSenators.php?id=<?php echo $data[0]; ?>">Edit</a></td>
          </tr>
          <?php endwhile; ?>
        </table>
      </div>
   </div>
 </div>
</div>
</center>
<footer>
  <center>
    <br />
  <span>©2017 Donald Elliott & Sarah Maas</span><br />
  <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
  <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
  <br />
</center>
</footer>
<!-- Constraint on active status -->
<script>
$('input[type="checkbox"]').on('change', function() {
 $(this).siblings('input[type="checkbox"]').prop('checked', false);
});
</script>
    </body>
    </html>
