<?php
/*
Filename:     addSponsor.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to display the sponsors and add to the database
*/
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    $bill_id = $_POST['bill_id'];
    $senator_id = $_POST['senator_id'];
    $sqlinsert = "INSERT INTO sponsors (bill_id, senator_id) VALUES ('$bill_id', '$senator_id')";
    $insert = $dbcon->query($sqlinsert);
    if(!$insert) {
      die("Error: {$dbcon->errno} : {$dbcon->error}");
    }
  }
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/sponsor.php");
?>
