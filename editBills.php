<?php
/*
Filename:     editBills.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to allow editing of bills
              within the database
*/
  include('dbhook.php');
  $id = $_GET["id"];
  //Get the name from bill specified from bill.php
  $sqlBillName = "SELECT name FROM bills WHERE id ='$id'";
  $query = mysqli_query($dbcon, $sqlBillName);
  $bill = mysqli_fetch_array($query);
  $sqlAllBill = "SELECT * FROM bills WHERE id='$id'";
  $newQuery = mysqli_query($dbcon, $sqlAllBill);
?>
<!-- BEGIN HTML -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>CS340 Final Project</title>
    <meta name="description" content="CS340 Database Project">
    <meta name="author" content="Sarah Maas & Donald Elliott">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONT -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <!-- SCRIPTS -->
    <script src="js/jquery.js"></script>
    <script src="js/typed.js"></script>
    <script src="js/showHide.js"></script>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="images/favicon.png">
  </head>
  <body>
    <!-- Page Layout -->
    <!-- Navigation Menu -->
  <ul>
    <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
    <li><a href="party.php" style="cursor:pointer;">Party</a></li>
    <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
    <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
    <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
    <li><a href="state.php" style="cursor:pointer;">States</a><li>
    <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
  </ul>
  <center>
    <div class="container">
      <!-- Display the bill name that is being edited -->
      <h3>Edit <?php echo $bill[0]; ?></h3>
      <table class="table table-striped">
        <tr>
          <th>ID</th>
          <th>Name</th>
        </tr>
        <?php while($data = mysqli_fetch_array($newQuery)):; ?>
        <tr>
          <!-- DISPLAY THE BILL ID AND NAME BEING EDITED IN A TABLE -->
          <td><?php echo $data[0]; ?></td>
          <td><?php echo $data[1]; ?></td>
        </tr>
        <?php endwhile; ?>
      </table>
      <form method="post" action="updateBill.php">
        <input type="hidden" name="submitted" value="true" />
        <input type="hidden" name="id" value="<?php echo htmlspecialchars($_GET["id"]); ?>" />
        <div class="form-group">
          <!-- FORM FOR EDITING THE BILL NAME -->
          <label for="inputBillName">Edit Bill Name</label><br />
          <input type="text" name="name" class="form-control" id="inputBillName" placeholder="">
        </div>
        <button type="submit" class="btn btn-default">Update Bill</button>
      </form>
    </div>
    <footer>
      <center>
        <br />
      <span>©2017 Donald Elliott & Sarah Maas</span><br />
      <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
      <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
      <br />
    </center>
    </footer>
  </center>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
