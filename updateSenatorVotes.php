<?php
/*
Filename:     updateSenatorVotes.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Updates the senator votes.
*/
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Set names passed in to variables
    $id = $_POST['id'];
    $yeas = $_POST['yeas'];
    $nays = $_POST['nays'];
    $no_vote = $_POST['no_vote'];
    //Update the yeas
    if ($yeas != "") {
      $sqlEdit = "UPDATE senator_votes SET yeas='$yeas' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
    //Update the nays
    if ($nays != "") {
      $sqlEdit = "UPDATE senator_votes SET nays='$nays' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
    //Update the no votes
    if ($no_vote != "") {
      $sqlEdit = "UPDATE senator_votes SET no_vote='$no_vote' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
  }
  //Once all updated refresh to senatorVote.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/senatorVote.php");
?>
