<?php
/*
Filename:     editParty.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to edit parties within the database
*/
  include('dbhook.php');
  $id = $_GET["id"];
  //Get the party name from parties specified by user from party.php
  $sqlPartyName = "SELECT name FROM parties WHERE id ='$id'";
  $query = mysqli_query($dbcon, $sqlPartyName);
  $parties = mysqli_fetch_array($query);
  $sqlAllParties = "SELECT * FROM parties WHERE id='$id'";
  $newQuery = mysqli_query($dbcon, $sqlAllParties);
?>
<!-- START HTML -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>CS340 Final Project</title>
    <meta name="description" content="CS340 Database Project">
    <meta name="author" content="Sarah Maas & Donald Elliott">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONT -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <!-- SCRIPTS -->
    <script src="js/jquery.js"></script>
    <script src="js/typed.js"></script>
    <script src="js/showHide.js"></script>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="images/favicon.png">
  </head>
  <body>
    <!-- Page Layout -->
    <!-- Navigation Menu -->
  <ul>
    <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
    <li><a href="party.php" style="cursor:pointer;">Party</a></li>
    <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
    <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
    <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
    <li><a href="state.php" style="cursor:pointer;">States</a><li>
    <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
  </ul>
  <center>
    <div class="container">
      <!-- DISPLAY THE NAME OF THE PARTY TO BE EDITED -->
      <h3>Edit <?php echo $parties[0]; ?></h3>
      <table class="table table-striped">
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Abbreviation</th>
        </tr>
        <?php while($data = mysqli_fetch_array($newQuery)):; ?>
        <tr>
          <!-- DISPLAY THE INFORMATION FROM THE PARTY FIELD -->
          <td><?php echo $data[0]; ?></td>
          <td><?php echo $data[1]; ?></td>
          <td><?php echo $data[2]; ?></td>
        </tr>
        <?php endwhile; ?>
      </table>
      <form method="post" action="updateParty.php">
        <input type="hidden" name="submitted" value="true" />
        <input type="hidden" name="id" value="<?php echo htmlspecialchars($_GET["id"]); ?>" />
        <div class="form-group">
          <!-- FORM TO ALLOW EDITING OF THE PARTY NAME AND ABBREVIATION -->
          <label for="inputPName">Edit Party Name</label><br />
          <input type="text" name="name" class="form-control" id="inputPName" placeholder=""><br />
          <label for="inputABBR">Edit Party Abbreviation</label><br />
          <input type="text" name="abbreviation" class="form-control" id="inputABBR" placeholder=""><br />
        </div>
        <button type="submit" class="btn btn-default">Update Party</button>
      </form>
    </div>
    <footer>
      <center>
        <br />
      <span>©2017 Donald Elliott & Sarah Maas</span><br />
      <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
      <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
      <br />
    </center>
    </footer>
  </center>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
