<?php
/*
Filename:     filter.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Allows for the filtering of votes on a bill
              down the party lines.
*/
ini_set('display_errors', 'On');
include('dbhook.php');
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PSWD, DB_NAME);
?>
<!-- Start the HTML -->
<!DOCTYPE html>
<html>
  <head>
		<meta charset="utf-8">
		<title>CS340 Final Project</title>
		<meta name="description" content="CS340 Database Project">
		<meta name="author" content="Sarah Maas & Donald Elliott">
		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- FONT -->
		<link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
		<!-- CSS -->
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/font-awesome.css">
		<!-- SCRIPTS -->
		<script src="js/jquery.js"></script>
		<script src="js/typed.js"></script>
		<script src="js/showHide.js"></script>
		<!-- Favicon -->
		<link rel="icon" type="image/png" href="images/favicon.png">
	</head>
<body>
  <ul>
    <li><a href="/~elliotdo/DB/adminPage.php">Add Data</a></li>
    <li><a href="/~elliotdo/DB/authors.html">Authors</a></li>
    <li><a href="/~elliotdo/DB/index.php">Home</a></li>
  </ul>
  <center>
<div class="container" id="hideTable">
  <table class="svg-attribute table-bordered text-center">
    <h3>Vote Split</h3>
    <thead>
		<tr>
			<th>Bill</th>
			<th>Party</th>
			<th>Yeas</th>
			<th>Nays</th>
			<th>No Vote</th>
		</tr>
    </thead>
    <?php
      $bill_id = $_POST['Bill'];
      if($bill_id == -1) {
	       $bill_id = '%';
       }
       /*
        SQL Statement is used to grab all the information from bills, senators,
        and parties in order to provide a filter to the user from index.php. It also
        totals the values from yeas, nays, no votes based upon the senator's party.
       */
       if(!($stmt = $mysqli->prepare("SELECT b.name bill,
					CONCAT(  '   ', p.name ) party,
					SUM( yeas ) yeas, SUM( nays ) nays,
					SUM( no_vote ) no_vote
					FROM senator_votes v
					INNER JOIN senators s ON v.senator_id = s.id
				  INNER JOIN parties p ON s.party_id = p.id
					INNER JOIN bills b ON v.bill_id = b.id
					WHERE b.id LIKE  ?
					GROUP BY b.name, p.name
					UNION
				  SELECT b.name,
					'Total',
					SUM( yeas ) yeas,
					SUM( nays ) nays,
				  SUM( no_vote ) no_vote
					FROM senator_votes v
					INNER JOIN senators s ON v.senator_id = s.id
					INNER JOIN parties p ON s.party_id = p.id
					INNER JOIN bills b ON v.bill_id = b.id
					WHERE b.id LIKE  ?
					GROUP BY b.name
					ORDER BY 1 , 2"))){
					       echo "Prepare failed: "  . $stmt->errno . " " . $stmt->error;
					}
          if(!($stmt->bind_param("ss", $bill_id, $bill_id))){
	           echo "Bind failed: "  . $stmt->errno . " " . $stmt->error;
          }
          if(!$stmt->execute()){
	           echo "Execute failed: "  . $mysqli->connect_errno . " " . $mysqli->connect_error;
          }
          if(!$stmt->bind_result($bill, $party, $yeas, $nays, $no_vote)){
	           echo "Bind failed: "  . $mysqli->connect_errno . " " . $mysqli->connect_error;
          }
          while($stmt->fetch()){
            echo "<tr><td>" . $bill . "</td><td>" . $party . "</td><td>" . $yeas . "</td><td>" . $nays . "</td><td>" . $no_vote . "</td></tr>";
          }
          $stmt->close();
        ?>
	</table>
</div>
<div>
<form method="post" action="filter.php">
  <fieldset>
    <legend>Filter By Bill</legend>
      <select name="Bill">
        <?php
        //Statements to alert of failures.
        if(!($stmt = $mysqli->prepare("SELECT id, name FROM bills"))){
          echo "Prepare failed: "  . $stmt->errno . " " . $stmt->error;
        }
        if(!$stmt->execute()){
          echo "Execute failed: "  . $mysqli->connect_errno . " " . $mysqli->connect_error;
        }
        if(!$stmt->bind_result($id, $pname)){
          echo "Bind failed: "  . $mysqli->connect_errno . " " . $mysqli->connect_error;
        }
         echo '<option value=" -1 "> ' . 'SHOW ALL' . '</option>\n';
        while($stmt->fetch()){
         echo '<option value="'. $id . '"> ' . $pname . '</option>\n';
        }
        $stmt->close();
        ?>
      </select>
  </fieldset>
  <input type="submit" value="Run Filter" /><br />
  <p style="color:#09a2ff;">AN EMPTY TABLE MEANS THE BILL HAS NOT YET BEEN VOTED ON</p>
</form>
</div>
<footer>
  <center>
    <br />
  <span>©2017 Donald Elliott & Sarah Maas</span><br />
  <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
  <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a><br />
  <br />
</center>
</footer>
</center>
 <script>
		var cell = document.getElementsByTagName("td");
		for (var i = 0; i < cell.length; i++) {
		  if(cell[i].innerHTML == "Total") {
		    cell[i].parentNode.classList.add("totals-row");
  }
}
	</script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
