<?php
/*
Filename:     updateSenator.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to update the senator.
*/
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Set names passed in to variables
    $id = $_POST['id'];
    $party_id = $_POST['party_id'];
    $state_id = $_POST['state_id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $active = $_POST['active'];
    //Update the party id
    if ($party_id != "") {
      $sqlEdit = "UPDATE senators SET party_id='$party_id' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
    //Update the state id
    if ($state_id != "") {
      $sqlEdit = "UPDATE senators SET state_id='$state_id' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
    //Update the first name
    if ($first_name != "") {
      $sqlEdit = "UPDATE senators SET first_name='$first_name' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
    //Update the last name
    if ($last_name != "") {
      $sqlEdit = "UPDATE senators SET last_name='$last_name' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
    //Update the active status
    if ($active != "") {
      $sqlEdit = "UPDATE senators SET active='$active' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
  }
  //Once all updated refresh to senators.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/senators.php");
?>
