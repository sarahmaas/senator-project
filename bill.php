<?php
/*
Filename:     bill.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to display the bills in the database and
              also allows users to select to edit a bill.
*/
  include('dbhook.php');
  //Select all the bills and order them by their ID
  $sqlselect = "SELECT * FROM bills
                 ORDER BY bills.id";
  $result = mysqli_query($dbcon, $sqlselect);
?>
<!--BEGIN HTML-->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="js/showHide.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <!-- Page Layout -->
  <!-- Navigation Menu -->
<ul>
  <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
  <li><a href="party.php" style="cursor:pointer;">Party</a></li>
  <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
  <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
  <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
  <li><a href="state.php" style="cursor:pointer;">States</a><li>
  <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
</ul>
<center>
<div class="container">
  <div class="row">
      <div class="twelve.columns">
        <!--
          Start the Add Bill Section. Will use a post and utilize addBill.php
        -->
        <h2>Add Bill</h2>
        <form method="post" action="addBill.php">
          <input type="hidden" name="submitted" value="true" />
          <div class="form-group">
            <label for="billName">Name</label><br />
            <input type="text" name="name" class="form-control" id="billName" placeholder="Bill">
          </div>
          <button type="submit" class="btn btn-default">ADD BILL</button>
        </form>
        <div id="showTheBills">
        <h3>Bills</h3>
        <table>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
          </tr>
          <?php while($data = mysqli_fetch_array($result)):; ?>
          <tr>
            <!--Get the Bill ID from the array-->
            <td><?php echo $data[0]; ?></td>
            <!--Get the Bill Name from the array-->
            <td><?php echo $data[1]; ?></td>
            <!--Send the ID of the bill to editBills.php for editing-->
            <td><a href="editBills.php?id=<?php echo $data[0]; ?>">Edit</a></td>
          </tr>
          <?php endwhile; ?>
        </table>
      </div>
      </div>
    </div>
  </div>
</center>
  <footer>
    <center>
      <br />
    <span>©2017 Donald Elliott & Sarah Maas</span><br />
    <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
    <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
  </center>
  </footer>
</body>
</html>
