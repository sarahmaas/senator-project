<?php
/*
Filename:     state.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to display the state information.
*/
  include('dbhook.php');
  $sqlselect = "SELECT * FROM states";
  $result = mysqli_query($dbcon, $sqlselect);
?>
<!-- Start HTML -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="js/typed.js"></script>
  <script src="js/showHide.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <!-- Page Layout -->
  <!-- Navigation Menu -->
<ul>
  <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
  <li><a href="party.php" style="cursor:pointer;">Party</a></li>
  <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
  <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
  <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
  <li><a href="state.php" style="cursor:pointer;">States</a><li>
  <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
</ul>
<center>
<div class="container">
<div class="row">
      <div class="twelve.columns">
          <h2>Add States</h2>
          <!-- Using a post and addState.php to add items to DB -->
          <form method="post" action="addState.php">
            <input type="hidden" name="submitted" value="true" />
            <div class="form-group">
              <label for="stateName">Name</label><br />
              <input type="text" name="name" class="form-control" id="stateName" placeholder="New York">
            </div>
            <div class="form-group">
              <label for="stateAbbr">Abbreviation</label><br />
              <input type="text" name="abbreviation" class="form-control" id="stateAbbr" placeholder="NY">
            </div>
            <button type="submit" class="btn btn-default">ADD STATE</button>
          </form>
          <div id="showTheStates">
          <h3>States</h3>
          <table>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Abbreviation</th>
            </tr>
            <?php while($data = mysqli_fetch_array($result)):; ?>
            <tr>
              <!-- Display the data -->
              <td><?php echo $data[0]; ?></td>
              <td><?php echo $data[1]; ?></td>
              <td><?php echo $data[2]; ?></td>
            </tr>
            <?php endwhile; ?>
          </table>
        </div>
        </div>
      </div>
    </div>
  </center>
    <footer>
      <center>
        <br />
      <span>©2017 Donald Elliott & Sarah Maas</span><br />
      <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
      <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
      <br />
    </center>
    </footer>
  </body>
  </html>
