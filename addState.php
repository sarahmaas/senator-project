<?php
/*
Filename:     addState.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Adds a state to the database in the table states
*/
  //Get the submitted form
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Assign variable names from from names
    $name = $_POST['name'];
    $abbreviation = $_POST['abbreviation'];
    //Insert new states into the table
    $sqlinsert = "INSERT INTO states (name, abbreviation) VALUES ('$name', '$abbreviation')";
    $insert = $dbcon->query($sqlinsert);
    if(!$insert) {
      die("Error: {$dbcon->errno} : {$dbcon->error}");
    }
  }
  //Once state is added go back to state.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/state.php");
?>
