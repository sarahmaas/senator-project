<?php
/*
Filename:     search.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used for displaying the search items from index.php
*/
  include('dbhook.php');
  if (isset($_POST["query"])) {
    $searchVar = $_POST["query"];
  }
  else {
    echo "Shouldn't be getting this error message!";
  }
  //Query that will return the senators name, votes, what state they are from, and their party.
  //Using a variable input by the user on index.php to search the database for the proper records
  $query = "SELECT senators.first_name, senators.last_name, states.name, parties.name, bills.name, senator_votes.yeas, senator_votes.nays, senator_votes.no_vote FROM bills
    INNER JOIN senator_votes ON bills.id = senator_votes.bill_id
    INNER JOIN senators ON senator_votes.senator_id = senators.id
    INNER JOIN parties ON senators.party_id = parties.id
    INNER JOIN states ON senators.state_id = states.id
    WHERE senators.first_name LIKE '%$searchVar%' OR senators.last_name LIKE '%$searchVar%'
    ORDER BY senators.first_name";
  $searchQuery = mysqli_query($dbcon, $query);
?>
<!-- Start HTML -->
<!DOCTYPE html>
<html>
<head>
    <title>Search Results</title>
    <meta charset="utf-8">
    <meta name="description" content="CS340 Database Project">
    <meta name="author" content="Sarah Maas & Donald Elliott">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONT -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link href="lg-map/map.css" rel="stylesheet" type="text/css" />
    <!-- SCRIPTS -->
    <script src="js/jquery.js"></script>
    <script src="js/loadVotes.js"></script>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <ul>
    <li><a href="/~elliotdo/DB/adminPage.php">Add Data</a></li>
    <li><a href="/~elliotdo/DB/authors.html">Authors</a></li>
    <li><a href="/~elliotdo/DB/index.php">Home</a></li>
  </ul>
<center>
<h3>Search Results</h3>
<table>
  <tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>State</th>
    <th>Party</th>
    <th>Bill Name</th>
    <th>Yea</th>
    <th>Nay</th>
    <th>No Vote</th>
  </tr>
  <?php
  while($row = mysqli_fetch_array($searchQuery)):;
  ?>
  <tr>
    <!-- Display the search results -->
    <td><?php echo $row[0]; ?></td>
    <td><?php echo $row[1]; ?></td>
    <td><?php echo $row[2]; ?></td>
    <td><?php echo $row[3]; ?></td>
    <td><?php echo $row[4]; ?></td>
    <td><?php echo $row[5]; ?></td>
    <td><?php echo $row[6]; ?></td>
    <td><?php echo $row[7]; ?></td>
  </tr>
  <?php endwhile; ?>
</table>
<footer>
  <center>
    <br />
  <span>©2017 Donald Elliott & Sarah Maas</span><br />
  <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
  <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
  <br />
</center>
</footer>
</center>
</body>
</html>
