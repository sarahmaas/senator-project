<?php
/*
Filename:     addSenator.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Adds a senator to the database in the table senators
*/
  //Get the submitted form
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Assign form names to variables
    $party_id = $_POST['party_id'];
    $state_id = $_POST['state_id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $active = $_POST['active'];
    //Insert the new senator into the senators table
    $sqlinsert = "INSERT INTO senators (party_id, state_id, first_name, last_name, active) VALUES ('$party_id', '$state_id', '$first_name', '$last_name', '$active')";
    $insert = $dbcon->query($sqlinsert);
    if(!$insert) {
      die("Error: {$dbcon->errno} : {$dbcon->error}");
    }
  }
  //Once senator is added go back to senators.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/senators.php");
?>
