<?php
/*
Filename:     adminPage.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Main page for adding items to the database
              and also for editing some.
*/
  include('dbhook.php');
?>
<!--BEGIN HTML-->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="js/typed.js"></script>
  <script src="js/showHide.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<!-- Page Layout -->
<body>
  <!-- Navigation Menu -->
<ul>
  <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
  <li><a href="party.php" style="cursor:pointer;">Party</a></li>
  <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
  <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
  <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
  <li><a href="state.php" style="cursor:pointer;">States</a><li>
    <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
</ul>
<center>
  <!--Display a message to choose menu items to interact with the DB-->
  <div class="container" id="welcomePic">
  <div class="row">
        <div class="twelve.columns">
          <h1 class="Raleway">Choose Your Option Above To Add Items</h1>
          </div>
        </div>
      </div>
  </center>
  <footer>
    <center>
      <br />
    <span>©2017 Donald Elliott & Sarah Maas</span><br />
    <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
    <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
  </center>
  </footer>
      </body>
      </html>
