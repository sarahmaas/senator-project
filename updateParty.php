<?php
/*
Filename:     updateParty.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Updates the party information.
*/
  if(isset($_POST['submitted'])) {
    include('dbhook.php');
    //Set names passed in to variables
    $id = $_POST['id'];
    $name = $_POST['name'];
    $abbreviation = $_POST['abbreviation'];
    //Update the name
    if ($name != "") {
      $sqlEdit = "UPDATE parties SET name='$name' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
    //Update the abbreviation
    if ($abbreviation != "") {
      $sqlEdit = "UPDATE parties SET abbreviation='$abbreviation' WHERE id='$id'";
      $update = $dbcon->query($sqlEdit);
      if(!$update) {
        die("Error: {$dbcon->errno} : {$dbcon->error}");
      }
    }
  }
  //Once both are updated refresh back to party.php
  header("Location:http://web.engr.oregonstate.edu/~elliotdo/DB/party.php");
?>
