<?php
/*
Filename:     senatorVote.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Displays the senator voting
*/
  include('dbhook.php');
  $sqlselect = "SELECT senators.id, senators.first_name, senators.last_name, bills.id, bills.name, senator_votes.yeas, senator_votes.nays, senator_votes.no_vote FROM bills
              INNER JOIN senator_votes ON bills.id = senator_votes.bill_id
              INNER JOIN senators ON senator_votes.senator_id = senators.id
              ORDER BY senators.id";
  $result = mysqli_query($dbcon, $sqlselect);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CS340 Final Project</title>
  <meta name="description" content="CS340 Database Project">
  <meta name="author" content="Sarah Maas & Donald Elliott">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT -->
  <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <!-- SCRIPTS -->
  <script src="js/jquery.js"></script>
  <script src="js/typed.js"></script>
  <script src="js/showHide.js"></script>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <!-- Page Layout -->
  <!-- Navigation Menu -->
<ul>
  <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
  <li><a href="party.php" style="cursor:pointer;">Party</a></li>
  <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
  <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
  <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
  <li><a href="state.php" style="cursor:pointer;">States</a><li>
  <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
</ul>
<center>
<div class="container">
  <div class="row">
      <div class="twelve.columns">
        <h2>Senator Votes</h2>
        <!--Using a post and addSenVote.php to add items to DB-->
        <form method="post" action="addSenVote.php">
          <input type="hidden" name="submitted" value="true" />
          <div class="form-group">
            <label for="senatorID">Senator ID</label><br />
            <input type="text" name="senator_id" class="form-control" id="senatorID" placeholder="Senator ID #">
          </div>
          <div class="form-group">
            <label for="billID">Bill ID</label><br />
            <input type="text" name="bill_id" class="form-control" id="billID" placeholder="Bill ID #">
          </div>
          <center>
            <!--Utilizing a checkbox to make vote selection easier-->
            <!--Constraints are handled via JS (ie, you can only check one box)-->
            <h5>How Did They Vote?</h5>
            <input type="checkbox" id="yea" class="small" value="1" name="yeas">
            <label for="yea">Yea</label></input>
            <input type="checkbox" id="nay" class="small" value="1" name="nays">
            <label for="nay">Nay</label></input>
            <input type="checkbox" id="noVote" class="small" value="1" name="no_vote">
            <label for="noVote">No Vote</label></input>
          </center>
          <button type="submit" class="btn btn-default" onClick"refreshSenVote()"=>ADD VOTES</button>
        </form>
            <div id="showTheSenVotes">
        <h3>Vote Counts</h3>
        <table>
          <tr>
            <th>Senator ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Bill ID</th>
            <th>Bill Name</th>
            <th>YEA</th>
            <th>NAY</th>
            <th>No Vote</th>
          </tr>
          <?php while($data = mysqli_fetch_array($result)):; ?>
          <tr>
            <!-- Display all the data -->
            <td><?php echo $data[0]; ?></td>
            <td><?php echo $data[1]; ?></td>
            <td><?php echo $data[2]; ?></td>
            <td><?php echo $data[3]; ?></td>
            <td><?php echo $data[4]; ?></td>
            <td><?php echo $data[5]; ?></td>
            <td><?php echo $data[6]; ?></td>
            <td><?php echo $data[7]; ?></td>
          </tr>
          <?php endwhile; ?>
        </table>
      </div>
      </div>
    </div>
  </div>
</center>
  <footer>
    <center>
      <br />
    <span>©2017 Donald Elliott & Sarah Maas</span><br />
    <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
    <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
    <br />
  </center>
  </footer>
  <!-- Script used to enforce constraints-->
  <script>
  $('input[type="checkbox"]').on('change', function() {
   $(this).siblings('input[type="checkbox"]').prop('checked', false);
});
  </script>
      </body>
      </html>
