<?php
/*
Filename:     editSenators.php
Authors:      Donald Elliott/Sarah Maas
Class:        CS340-400
Project:      Database Final Project
Description:  Used to allow editing of senators within
              the database.
*/
  include('dbhook.php');
  $id = $_GET["id"];
  //Getting the first name to display to user
  $sqlSenatorName = "SELECT first_name FROM senators WHERE id ='$id'";
  $query = mysqli_query($dbcon, $sqlSenatorName);
  $senators = mysqli_fetch_array($query);

  //Getting the second name to display to user
  $sqlSenatorLName = "SELECT last_name FROM senators WHERE id ='$id'";
  $query1 = mysqli_query($dbcon, $sqlSenatorLName);
  $senators1 = mysqli_fetch_array($query1);

  //Selecting senators(id, first name, last name, party id, state id, active)
  //Also selecting parties(name) and states(name)
  //Specified from senators.php, passed in as id
  $sqlAllSens = "SELECT senators.id, senators.first_name, senators.last_name, senators.party_id, parties.name, senators.state_id, states.name, senators.active FROM senators
                 INNER JOIN parties ON senators.party_id = parties.id
                 INNER JOIN states ON senators.state_id = states.id
                 WHERE senators.id='$id'";
  $newQuery = mysqli_query($dbcon, $sqlAllSens);
?>
<!-- START HTML -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>CS340 Final Project</title>
    <meta name="description" content="CS340 Database Project">
    <meta name="author" content="Sarah Maas & Donald Elliott">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONT -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <!-- SCRIPTS -->
    <script src="js/jquery.js"></script>
    <script src="js/typed.js"></script>
    <script src="js/showHide.js"></script>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="images/favicon.png">
  </head>
  <body>
    <!-- Page Layout -->
    <!-- Navigation Menu -->
  <ul>
    <li><a href="sponsor.php" style="cursor:pointer;">Sponsors</a></li>
    <li><a href="party.php" style="cursor:pointer;">Party</a></li>
    <li><a href="senatorVote.php" style="cursor:pointer;">Votes</a></li>
    <li><a href="bill.php" style="cursor:pointer;">Bills</a></li>
    <li><a href="senators.php" style="cursor:pointer;">Senators</a></li>
    <li><a href="state.php" style="cursor:pointer;">States</a><li>
    <li><a href="adminPage.php" style="cursor:pointer;">Admin Home</a><li>
  </ul>
  <center>
    <div class="container">
      <!-- DISPLAY THE FIRST AND LAST NAME OF THE SENATOR TO BE EDITED -->
      <h3>Edit <?php echo $senators[0]; ?><?php echo ' ';?><?php echo $senators1[0]; ?></h3>
      <table class="table table-striped">
        <tr>
          <th>ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Party ID</th>
          <th>Party</th>
          <th>State ID</th>
          <th>State</th>
          <th>Active</th>
        </tr>
        <?php while($data = mysqli_fetch_array($newQuery)):; ?>
        <tr>
          <!-- DISPLAY THE TABLE INFORMATION IN THE FIELDS ABOVE (th) -->
          <td><?php echo $data[0]; ?></td>
          <td><?php echo $data[1]; ?></td>
          <td><?php echo $data[2]; ?></td>
          <td><?php echo $data[3]; ?></td>
          <td><?php echo $data[4]; ?></td>
          <td><?php echo $data[5]; ?></td>
          <td><?php echo $data[6]; ?></td>
          <td><?php echo $data[7]; ?></td>
        </tr>
        <?php endwhile; ?>
      </table>
      <!-- USING A POST AND updateSenator.php for edit -->
      <form method="post" action="updateSenator.php">
        <input type="hidden" name="submitted" value="true" />
        <input type="hidden" name="id" value="<?php echo htmlspecialchars($_GET["id"]); ?>" />
        <div class="form-group">
          <!-- FORM TO ALLOW THE EDITING OF THE SENATORS -->
          <label for="editFN">Edit First Name</label><br />
          <input type="text" name="first_name" class="form-control" id="editFN" placeholder=""><br />
          <label for="editLN">Edit Last Name</label><br />
          <input type="text" name="last_name" class="form-control" id="editLN" placeholder=""><br />
          <label for="editPID">Edit Party ID</label><br />
          <input type="text" name="party_id" class="form-control" id="editPID" placeholder=""><br />
          <label for="editSID">Edit State ID</label><br />
          <input type="text" name="state_id" class="form-control" id="editSID" placeholder=""><br />
          <label for="editStatus">Edit Active Status</label><br />
          <input type="checkbox" id="editStatus" class="small" value="1" name="active">
          <label for="yea">Active</label></input>
          <input type="checkbox" id="editStatus" class="small" value="0" name="active">
          <label for="nay">Inactive</label></input>
          </div>
        <button type="submit" class="btn btn-default">Update Senator</button>
      </form>
    </div>
    <footer>
      <center>
        <br />
      <span>©2017 Donald Elliott & Sarah Maas</span><br />
      <a href="/~elliotdo/DB/index.php" class="white-text">Home</a>
      <a href="/~elliotdo/DB/authors.html" class="white-text">Authors</a>
      <br />
    </center>
    </footer>
  </center>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
